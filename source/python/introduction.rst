********************************
Python: Chapter 1 - Introduction
********************************

A programming language is a tool that helps us write computer software , using a collection of commands stored inside a text file we call "source code". But how exactly do programming languages work?

1.1 Machine code
================

Machine code is the only computer language our machine can understand. 

2.2 Compiled vs Intepreted
==========================

A compiler takes source code and converts it to machine code while an intepreter does not perform any convertion, instead it uses our source code to trigger specific machine code already generated for the language and its libraries. 
